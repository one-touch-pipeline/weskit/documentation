# A Webinterface for the GA4GH compliant Workflow-Execution-Service WESkit.

The WESkit [dashboard](https://gitlab.com/one-touch-pipeline/weskit/dashboard) provides visual inspection of the WESkit runs and results for users.
The dashboards supports OICD login and communicates to the WESkit-API and the OICD provider.
The latest container image is available here: `registry.gitlab.com/one-touch-pipeline/weskit/dashboard:latest`.

## Configuration

The WESkit dashboard is configured via environmental variables.
A complete list with description is given below:

Required:
- **WESKIT_DASHBOARD_URL**:  URL of the WESkit server.
- **WESKIT_API_URL**: URL of the dashboard website.
- **WESKIT_INFO__info_text**: A short description, which is shown at the info site.
- **WESKIT_INFO__imprint_name**: A name used for the website imprint
- **WESKIT_INFO__imprint_address** Address used for the website imprint
- **WESKIT_INFO__imprint_email** E-Mail address used for the website imprint
- **WESKIT_LOGIN**: Boolean value if OIDC login is activated for the dashboard.

Optional, required for OIDC login:
- **WESKIT_JWT_COOKIE_SECURE**: flask-jwt-extended (option)[https://flask-jwt-extended.readthedocs.io/en/stable/options/]
- **WESKIT_JWT_TOKEN_LOCATION**: flask-jwt-extended (option)[https://flask-jwt-extended.readthedocs.io/en/stable/options/]
- **WESKIT_JWT_ALGORITHM**: flask-jwt-extended (option)[https://flask-jwt-extended.readthedocs.io/en/stable/options/]
- **WESKIT_JWT_DECODE_AUDIENCE**: flask-jwt-extended (option)[https://flask-jwt-extended.readthedocs.io/en/stable/options/]
- **WESKIT_JWT_IDENTITY_CLAIM**: flask-jwt-extended (option)[https://flask-jwt-extended.readthedocs.io/en/stable/options/]
- **WESKIT_OIDC_ISSUER_URL**: URL for OIDC issuer
- **WESKIT_OIDC_CLIENTID**: OIDC client identifier
- **WESKIT_OIDC_CLIENT_SECRET**: OIDC client secret
- **WESKIT_OIDC_REALM**: OIDC realm

## Running as single container

You can run a single dashboard container e.g. next to WESkit by:

```bash
docker run --rm \
  -p 5001:5001 \
  -e WESKIT_DASHBOARD_URL="https://localhost:5001" \
  -e WESKIT_API_URL=$WESKIT_API_URL \
  -e WESKIT_LOGIN="false" \
  -e WESKIT_INFO__info_text="This is a demo website" \
  -e WESKIT_INFO__imprint_name="My Name" \
  -e WESKIT_INFO__imprint_address="Somestreet 13, Somecity 12345" \
  -e WESKIT_INFO__imprint_email="bla@blub.com" \
  -w="/dashboard" \
  registry.gitlab.com/one-touch-pipeline/weskit/dashboard:latest \
  bash -i -c "conda activate dashboard && uwsgi --ini /dashboard/uwsgi_server/uwsgi.ini"
```

## Running in stack

The dashboard is also included in the WESkit demo deployment stack.
Please see the [demo deployment](https://gitlab.com/one-touch-pipeline/weskit/deployment) page for running the dashboard within a docker stack.

