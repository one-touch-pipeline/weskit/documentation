# Architecture

WESkit is deployed with most of its auxiliary services in Docker containers. Containers are deployed with Docker Swarm (old way, unmaintained), but we are in the progress of migrating the deployment away from Swarm and instead use Helm and Kubernetes.

The following types of containers are deployed

 * WESkit: The core WESkit WES-API server.
 * MongoDB: Persistence for WESkit. Here are the metadata for the workflow runs stored.
 * Redis: The message queue and result broker used by Celery.
 * Worker containers: Each container runs a Celery worker.

Note that WESkit requires access to a shared filesystem from the WESkit REST-server and the workers containers. For development this can be a local directory, for production this will usually be an NFS storage, such as a central NFS of your organisation or shared filesystem deployed on the cloud infrastructure that runs the services and compute infrastructure. In the following diagrams, components that require NFS access are indicated with `<<NFS>>`. 

Additionally, there are some optional services (for Docker Swarm, WIP for the Kubernetes deployment):

 * Traefik: Reverse proxy that exposes the outside API
 * Keycloak: AAI service useful for testing and stand-alone deployments
 * MySQL: Persistence for Keycloak

The computations by the workflows can in principle be done in the worker containers, but as these computations can have very large resource requirements this is not advisable for more than testing purposes. Generally, you want to use an external compute infrastructure. The following diagram shows the basic deployment. Components with `<<NFS>>` mount a shared filesystem. The components marked with `<<swarm>>` are currently only supported by the old Docker Swarm deployment. In the Swarm deployment they are also optional.

```plantuml
@startuml
scale 650*800

cloud Cloud {


    () WES

    () S3

    () NFS

    component "MinIO NAS Gateway" as NFSGateway <<NFS>>, <<swarm>>
    S3 - NFSGateway

    component "NFS Storage" as NFSstorage
    NFS - NFSstorage

    NFSGateway -down- NFSstorage
    
    NFSstorage -[hidden]down- Host1

    node "host 1" as Host1 {

        () WES
        component WESkit <<NFS>>

        database MongoDB
        WESkit -up- MongoDB

        component Redis
        note top: result broker
        WESkit -right- Redis
        
        component Traefik <<swarm>>
        WES -right- Traefik
        Traefik -right- WESkit
    }

    node "hosts 2+" as Hosts {

        component CeleryWorkerN <<NFS>>
        Redis -down- CeleryWorkerN

        component CeleryWorker1 <<NFS>>
        Redis -down- CeleryWorker1

        CeleryWorker1 -[hidden]right- CeleryWorkerN
    }
}
@enduml
```

## Running Workflows

Currently (2023-10-19), workflow engines can be run in the Celery worker containers, or in SLURM or LSF clusters.
We are working on a Kubernetes executor, which will allow to run workflow engines in Kubernetes clusters.
WESkit supports multiple workflow engines by executing their executables natively.
Consequently, the workflow engines are responsible for managing the submission of compute/workload jobs (or "tasks") to whatever compute infrastructure they support. 
This is illustrated in the following sections.

### Workflow engine processes in celery workers 

The following diagram illustrates the deployment in which the workflow engines (i.e. Nextflow or Snakemake control processes) run within in the Celery workers.

Each workflow-manager instances, i.e. the Nextflow, Snakemake, etc., is run as a separate process within the worker containers (with `subprocess` in Python).

```plantuml
@startuml
scale 650*800

cloud OpenStack {

    () WES
        
    node "Docker Swarm Host 1" as Swarm1 {

        component WESkit <<NFS>>
        WES -right- WESkit

        database MongoDB
        WESkit -up- MongoDB

        component Redis
        WESkit -right- Redis

    }

    node "Docker Swarm Host 2+" as WorkerSwarm {

        component CeleryWorkerN <<NFS>> {
            component Snakemake
        }
        Redis -down- CeleryWorkerN

        component CeleryWorker1 <<NFS>> {
            component Nextflow
        }
       
        Redis -down- CeleryWorker1

        CeleryWorker1 -[hidden]right- CeleryWorkerN
    }


}

note left of CeleryWorker1
Each worker may run multiple
engines via gevent-based
concurrency.
endnote

frame "Batch Processing Cluster" as Cluster {

    node SubmissionHost <<NFS>>
    note right
      Components with <<NFS>> have access
      to the same shared filesystems
    end note
    Snakemake -down- SubmissionHost : SSH or REST
    Nextflow -down- SubmissionHost : SSH or REST

    node ComputeNode.a <<NFS>> {
      component Task.1
      SubmissionHost -down- Task.1
    }
    node ComputeNode.b <<NFS>> {
      component Task.N
      SubmissionHost -down- Task.N
    }

}
Swarm1 --[hidden]down-- Cluster

@enduml
```

The disadvantage of this deployment is that possibly long-running workflows depend on the continued availability of the Celery workers and the cluster, which increases the probability of workflows getting terminated by infrastructure problems. Furthermore, as WESkit does not support engine reruns (and this topic itself is also not well covered by the GA4GH WES API), the costs of failure are a complete rerun of the interrupted workflows. Finally, WESkit does not include any scheduling or accounting features.

### Workflow engine processes are run in the compute infrastructure

In this deployment option, WESkit submits the workflow engines into an external batch processing cluster. This currently works for IBM LSF and SLURM. The engine process itself is submitted as job into the cluster, and then submits its workload tasks (dependent on the engine's configuration) again in the same cluster.

```plantuml
@startuml
scale 650*1000

cloud OpenStack {

    () WES
        
    node "Docker Swarm Host 1" as Swarm1 {

        component WESkit <<NFS>>
        WES -right- WESkit

        database MongoDB
        WESkit -up- MongoDB

        component Redis
        note top: result broker
        WESkit -right- Redis

    }

    node "Docker Swarm Host 2+" as WorkerSwarm {

        component CeleryWorkerN <<NFS>>
        Redis -down- CeleryWorkerN

        component CeleryWorker1 <<NFS>>
        Redis -down- CeleryWorker1

        CeleryWorker1 -[hidden]right- CeleryWorkerN
    }


}

frame "Batch Processing Cluster" as Cluster {

    node SubmissionHost <<NFS>>
    note right
      Components with <<NFS>> have access
      to the same shared filesystem
    end note
    CeleryWorker1 -down-- SubmissionHost : SSH or REST
    CeleryWorkerN -down-- SubmissionHost : SSH or REST

    node ComputeNode.a <<NFS>> {
        component NextflowJob
        SubmissionHost -down- NextflowJob
    }

    node ComputeNode.b <<NFS>> {
        component SnakemakeJob
        SubmissionHost -down- SnakemakeJob
    }

    node ComputeNode.y <<NFS>> {
      component Task.1
      NextflowJob -down- Task.1
    }

    node ComputeNode.z <<NFS>> {
      component Task.N
      SnakemakeJob -down- Task.N
    }

}
Swarm1 --[hidden]down-- Cluster

@enduml
```

Offloading the workflow engine processes into the execution infrastructure, reduces the risk of workflow run interruptions. It becomes much easier to restart WESkit (e.g. for maintenance and updates). Furthermore, the execution infrastructure may support advanced scheduling and accounting support, which are out of scope for WESkit.


### Kubernetes-based workflow execution

In WESkit, the workflow engines are executed by an `Executor`, which is just a standardized internal API for running processes and keeping track of outputs and status. The executor can be changed to execute the workflow engine locally (`LocalExecutor`), via SSH (`SshExecutor`), or in a batch processing cluster (`LsfExecutor`, `SlurmExecutor`). Currently, we are working on a `KubernetesExecutor` that will allow to execute workflow engines in a Kubernetes cluster.

The following diagram shows this, and assumes that the workflow engine itself is also configured to submit its workload tasks into the same cluster. The diagram also shows the upcoming separation of the Celery worker type into a "submission" worker and one (or multiple) "monitoring" workers.

```plantuml
@startuml
scale 500*1000

() WES

cloud "Kubernetes Cluster 1" as Kubernetes1 {

    component WESkit <<NFS>>
    WES -down- WESkit

    database MongoDB
    WESkit -right- MongoDB

    component Redis
    WESkit -left- Redis

    component SubmissionWorkerN <<NFS>>
    Redis -down-> SubmissionWorkerN
    SubmissionWorkerN -up-> MongoDB

    component SubmissionWorker1 <<NFS>>
    Redis -down-> SubmissionWorker1
    SubmissionWorker1 -up-> MongoDB

    component MonitorWorker <<NFS>>
    Redis -right-> MonitorWorker
    MongoDB <-left- MonitorWorker

    SubmissionWorker1 -[hidden]right- SubmissionWorkerN
    SubmissionWorkerN -[hidden]right- MonitorWorker
    SubmissionWorker1 -[hidden]right- MonitorWorker
    
}

cloud "Kubernetes Cluster 2" as Kubernetes2 {

    component EnginePod.1 {
        component Nextflow
    }
    EnginePod.1 .up. SubmissionWorker1

    component EnginePod.M {
        component Snakemake
    }
    EnginePod.M .up. SubmissionWorkerN

    component Task.N1
    component Task.NK
    component Task.S1
    component Task.SL

    Nextflow -down-> Task.N1
    Nextflow -down-> Task.NK

    Snakemake -down-> Task.S1
    Snakemake -down-> Task.SL

}

@enduml
```

This deployment aims at cloud-based deployments. Note that currently WESkit can be configured with only a single `Executor`, which means, that all engines will be executed in the same Kubernetes cluster. Nevertheless, like displayed, the cluster for the engines may not be the same as the cluster for the WESkit deployment, which will be useful, if you want to deploy the WESkit services for production in managed VMs, but use an external compute infrastructure, such as a local OpenStack cloud, or AWS.