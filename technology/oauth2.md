The current WESkit Login System (04/21) is based on an oAuth2 authentication system. WESkit is using the identity provider ***KeyCloak*** as default and the only tested authentication backend.

# Token location

The access token has to be sent to the WESkit server via the HTTP header `Authorization`. The token has to be prefixed with `Bearer `.

WESkit does currently not support other token locations (e.g. cookies or query parameters).


# KeyCloak configuration.

This repository provides a working template of an KeyCloak configuration. The template can be used to test the setup, but it should be **never** used in a productive environment.

KeyCloak is part of the provided *docker stack*. But it is possible to use a external server to. Just comment the *keycloak* and *keycloak_mysql* section out and remove the *KeyCloak* dependency in the *rest* service.

The KeyCloak Admin Interface is accessible here: [https://localhost:8443/auth/](https://localhost:8443/auth/). Just use the KeyCloak Admin user to login.

# Users of the Demo Configuration
|  Role          | Username      | Password | Realm    |
| ---------------| ------------- | -------- | -------- |
| KeyCloak Admin | admin         | admin    | master   |


# Clients of the Demo Configuration
| Realm | ClientID         | ClientSecret          | Login Possible? |
| ---------------| ------------- | -------- | -------------- |
| WESkit | WESkit | `4ef0b5e7-9d43-44aa-9e82-63808c41a58e` | yes |


# Get *Access Token* from Keycloak

Note that if the following approaches fail for you, please check your [proxy](deployment/Proxies) settings. There may also be a problem with IPv4 and IPv6. See [here](Development#testing-the-running-rest-server) for more information.

## Via ***curl***

```bash
keycloak_host = "https://localhost:8443/realms/weskit/protocol/openid-connect/token"

grant_type="password"
username="test"
password="test"
client_id="OTP"
client_secret="7670fd00-9318-44c2-bda3-1a1d2743492d"  # example!

# Here comes the curl magic!
```

## Via ***Python*** requests
```python
import requests
import yaml


keycloak_host = "https://localhost:8443/auth/realms/WESkit/protocol/openid-connect/token"

Credentials = dict(
    grant_type="password",
    username="test",
    password="test",
    client_id="OTP",
    client_secret="7670fd00-9318-44c2-bda3-1a1d2743492d"
)

# Assuming that this is the path to the used KeyCloak certificate!
cert =  './uWSGI_Server/certs/weskit.pem'

# Request the Token
response = requests.post(url=hostname, data=credentials, verify=cert).json()

# Print the Result
print("Keycloak response:")
print(yaml.dump(response))

```
# Keycloak SSL encryption

In the default configuration the Keycloak server is protected with an SSL certificate for two domain names:

* localhost
* keycloak

The second domain "keycloak" is required to reach the keycloak server securely from the internal docker network.

Per default the same self-signed certificate is used for *WESkit* and *KeyCloak*. This avoids that the developer has to accept multiple certificates with their browser or interacting tool.

In the Docker Swarm deployment, if you use another certificate ensure that WESkit can access the docker secret 'keycloak.pem'.
