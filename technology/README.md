Here are information about WESkit's technologies, components and features.

  * [Architecture](technology/architecture.md)
  * [Login](technology/oauth2.md)
  * [Development](technology/development.md)
  * [Dashboard](technology/dashboard.md)
  * [Snakemake to TES](technology/snakemake_tes.md)

