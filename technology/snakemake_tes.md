Snakemake can be configured to use a TES (Task Execution Service) provider as computing backend. TES provider supported by ELIXIR cloud:
  - TESK without auth (CSC): https://csc-tesk-noauth.rahtiapp.fi/swagger-ui.html
  - TESK with auth (CSC): https://csc-tesk.rahtiapp.fi
  - TESK without auth (MUNI): https://tesk-na.cloud.e-infra.cz/swagger-ui.html
  - TESK with auth (MUNI): https://tesk-prod.cloud.e-infra.cz/swagger-ui.html

To use TES from Snakemake, all files need to be stored using s3-remote; otherwise the task execution won't be able to access the files, because a shared filesystem cannot be assumed. A minimal Snakemake example is given below:

```python
from snakemake.remote.S3 import RemoteProvider as S3RemoteProvider

localrules: all, download

S3 = S3RemoteProvider(
    endpoint_url="https://s3-elixir.cloud.e-infra.cz")

outfile_s3 = "ahm2022/bio-hack-test/outfile.txt"

rule all:
    input:
        file="ahm2022/bio-hack-test/outfile_local.txt"

rule processing:
    input:
        file=S3.remote("ahm2022/twardzso/indata.txt")
    output:
        file=S3.remote(outfile_s3)
    params:
        text="test text blablablubb"
    shell:
        """
        cp {input.file} {output.file}
        echo '\n' >> {output.file}
        echo {params.text} >> {output.file}
        """

rule download:
    input:
        file=S3.remote(outfile_s3)
    output:
        file="outfile_local.txt"
    shell:
        """
        sleep 10
        cp {input.file} {output.file}
        """
```

To run the example, first environmental variables need to be defined for accessing the S3 and for using conda environments. Additionally, for our example, the workflow will be executed in the /tmp directory of the container run in Kubernetes/TESK and its software stack, which is based on Conda, will also have to be configured to use that directory.

```console
export AWS_ACCESS_KEY_ID=MYID
export AWS_SECRET_ACCESS_KEY=MYKEY
export CONDA_PKGS_DIRS=/tmp/conda
export CONDA_ENVS_PATH=/tmp/conda
export HOME=/tmp
```

Execution (tested with Snakemake=7.18.2+12.gba3305b2):

```console
snakemake \
    --tes https://tesk-na.cloud.e-infra.cz \
    --use-conda \
    --envvars CONDA_PKGS_DIRS CONDA_ENVS_PATH AWS_ACCESS_KEY_ID AWS_SECRET_ACCESS_KEY HOME \
    --conda-prefix $CONDA_ENVS_PATH \
    --jobs 1 \
    --forceall \
    all
```

Snakemake will send the task to the TES and receive the task id. Information about tasks can be accessed from server, e.g. : `https://tesk-na.cloud.e-infra.cz/v1/tasks/task-40d39355?view=full`
