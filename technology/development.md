# Development

All repositories are under https://gitlab.com/one-touch-pipeline/weskit/. Please have a look at the [development](https://gitlab.com/groups/one-touch-pipeline/weskit/-/boards/2662726) board in Gitlab to see what we are currently working on. We are mostly communicating via [Slack](https://elixir-cloud.slack.com). 

We are in the process of migrating our Docker Swarm based deployment into Kubernetes. For this we use the `helm` tool. The following summarizes some aspects of the different deployments and the underlying technologies.

## Conda Environment

The WESkit service containers (REST, Celery workers) are based on Conda, which we use to install all necessary dependencies. For development of new features, it is recommended to install the WESkit Conda environment locally. This is useful for running the tests. To install the environment you need a working Conda installation (e.g. [Miniforge](https://github.com/conda-forge/miniforge)).

To install the environment you can then do

```bash
conda env create -n weskit -f environment.yaml
conda activate weskit
python -m pytest
```

Due to the fact that Conda package versions occasionally disappear from Conda channels, it may be possible that you won't be able to rebuild exactly this environment. In that case, you want to rebuild an environment from the `environment-minimal.yaml` as shown in the next section, which allows Conda to choose alternative package versions.

Note that the tests with workflows do not run the workflows in containers, but in the native environment created by Conda! For that reason the Conda environment needs to contain the workflow managers etc. 

Some tests are remote integration tests and need some configuration, if you want to run them. In any case, you should copy the `tests/remote-template.yaml` to `tests/remote.yaml` and (optionally) edit it.

### Updating the Conda Environment

There is an `environment-minimal.yaml` that contains only the necessary packages and tries to include few version constraints. This allows Conda to update the `environment.yaml` you should update the minimal environment file, build the environment, and export it into the `environment.yaml`

```bash
conda env create -n weskit-dev -f environment-minimal.yaml
conda env export -n weskit-dev > environment.yaml
```

If you are preparing a new `environment.yaml` that you want to release into our repository, you should furthermore, use the environment name "weskit" in the `environment.yaml`, make sure that the environment does not use packages from channels pulled in from your local `.condarc` (e.g. bioconda-legacy), and remove the prefix (directory) at the bottom of the file.

## Building the Docker container

We use two containers: A base container (`Dockerfile-base`) with all the software installed in it, and a thin outer container (`Dockerfile`), that includes the WESkit code and makes minor setups for the service containers. Both containers need to be build run WESkit in e.g. Kubernetes.

Note that by default, the Dockerfile creates a new user "weskit" and group "weskit" in the container, both with default IDs. For development, you can set the user ID and group ID in your locally built container to your own account's values.

```bash
docker build \
  --build-arg HTTP_PROXY=$HTTP_PROXY \
  --build-arg HTTPS_PROXY=$HTTPS_PROXY \
  --build-arg USER_ID=$(id -u) \
  --build-arg USER=$USER \
  --build-arg GROUP_ID=$(id -g) \
  --build-arg GROUP=$(groups | cut -f 1 -d ' ') \
  -t one-touch-pipeline/weskit/api/base:devel \
  -f Dockerfile-base
```

After that you can build the main image

```bash
docker build \
  -t one-touch-pipeline/weskit/api:devel
  -f Dockerfile
```

For production, the names and IDs should be the user and group that you want the container to have to access files on the relevant filesystems (dependent on deployment, local, NFS, etc.).

### Testing the running REST server

You can test the access via `curl` (easier if you have turned off SSL and authentication):

```bash
curl --ipv4 http://localhost:5000/ga4gh/wes/v1/service-info
```

> WARNING: Be careful that if you use Docker Swarm, then the server is currently only available via IPv4. If you have localhost set `::1` in your `/etc/hosts/`, a normal curl access to "http://localhost:5000" may not be possible (you probably get a redirect and then a permission denied if you use `-L` to follow the redirect). In that case, instead of "localhost" either user "127.0.01" as host to avoid hostname lookup, or us curl option `--ipv4` to enforce IPv4 access.

## Running WESkit directly

You also can run WESkit directly, without a container. Please have a look at the current Helm chart to get a list of environment variables needed for this. The following is just an outdated example: 

```bash
conda activate weskit 

# Configure the logger.
# export WESKIT_LOG_CONFIG=tests/log-config.yaml

# Path to redis configuration file. See `configs/redis.conf`
export REDIS_CONFIG=redis.yaml

# Path to WESkit configuration file. See `tests/weskit.yaml`. 
# Can also be provided via `--config`.
export WESKIT_CONFIG=tests/weskit.yaml

# Only for development. This is used for validating the weskit.yaml.
# export WESKIT_VALIDATION_CONFIG=tests/validation-config.yaml

# Path to the directory with the data to be processed. Defaults to `./data`.
export WESKIT_DATA=./data

# Then run the REST server
flask run --host=0.0.0.0 --port=5000
```

### Updating Keycloak

During testing, Keycloak is executed in a [testcontainer](https://github.com/testcontainers/testcontainers-python). The testcontainer mounts the `realm-export.json` and imports it into the Keycloak container. This will create the realm "weskit".

In case an upgrade requires a reconfiguration of Keycloak or the "weskit" realm, take the following steps:

1. Run a [keycloak/keycloak container](https://hub.docker.com/r/keycloak/keycloak) of the needed version.
2. For a fresh configuration of realm and client, do the following:
   1. Click on the "master" pull down in the left column and "Create Realm"
   2. Create a realm "weskit"
   3. Go to "Clients"
   4. Set "Client Id" to "weskit"
   5. Set "Name" to "weskit"
   6. Click "Next"
   7. Enable "Client authentication"
   8. In group "Authentication flow", enable "Service accounts role", and disable all others.
   9. Click "Next"
   10. Click "Save"
   11. Click tab "Creditials" (for client "weskit") and copy the "Client secret"
   12. Enter the client secret in the `test_keycloak` fixture in `conftest.py` at `IDP_CLIENT_SECRET`.
3. If you need to modify the realm, try to import it via the commandline in the container
   ```bash
   cat tests/keycloak/realm-export.json | docker exec -i mykeycloak /bin/bash -c "/opt/keycloak/bin/kc.sh import --file /dev/stdin"
   ```
3. After that, export the "weskit" realm from the running Keycloak container, and write it to the `tests/keycloak/realm-export.json` file.
   > NOTE: This has to be done from the commandline. A full export, including secrets is not possible via the UI.
   ```bash
   docker exec -it mykeycloak /bin/bash -c "/opt/keycloak/bin/kc.sh export --realm weskit --users same_file --file /tmp/realm-export.json > /dev/null; cat /tmp/realm-export.json;" > tests/keycloak/realm-export.json
   ```

Now, the tests should be able to run with the new Keycloak configuration.
