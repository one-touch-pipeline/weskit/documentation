# Docker-Compose Deployment

> NOTE: The Docker Compose deployment may be outdated and not work. It will only be updated, if needed.

The deployment script uses two [Docker compose files](https://docs.docker.com/compose/compose-file/).
1. The base-compose file contains all services required to run the stack on a standard machine. This file should not be edited by the users.
2. The user-compose file adapts the standard settings to the user requirements. The user-compose file can overwrite and adapt any settings from the base-compose file. Using the user-compose file is optional and can be set by using the `--compose` argument at the start:

```bash
python weskit_stack.py start --compose $USER_COMPOSE_FILE
```

## Base-compose file

The [base compose file](https://gitlab.com/one-touch-pipeline/weskit/deployment/-/blob/master/weskit_stack_base.yaml) contains standard settings which can be adapted and be overwritten by the user-compose file. The base stack consists of five different services.

- **rest**: the python flask app that receives the HTTP requests and communicates with the database and the celery worker. This app is developed by the weskit team and available in a weskit image.
- **database**: the MongoDB database stores all run information required by the weskit app.
- **result_broker**: Redis is used as a message queue between the flask app and the celery worker.
- **worker**: The celery workers execute the workflows and watch their status.
- **traefik**: Traefik provides TLS decryption and forwards incoming traffic into the rest service.

## User-compose file

To make customization as simple as possible for new users, the user-compose file offers only limited customizations options per default. Nevertheless, advanced user-compose files can be used by setting the `--expert_mode` flag at the start.

In user_mode (default mode) users can set:
- custom weskit/api images for the `rest` and the `worker` services.
- custom volumes for the `rest` and the `worker` services.

## Custom workflows

You can get more information on workflow installation and configuration [here](workflow-installation.md)
