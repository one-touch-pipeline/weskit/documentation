# WESkit Kubernetes Deployment

WESkit [provides](https://gitlab.com/one-touch-pipeline/weskit/helm-deployment) Helm charts for deployment into Kubernetes. These are still work in progress and may not contain all features that the original Docker Swarm deployment provided (reverse proxy, MinIO S3 endpoint, etc.).

Please refer to the [architecture section](../technology/architecture.md) for a general overview of the deployment. 

## **Developer** Deployment with Minikube

Minikube is a small and relatively easy to install Kubernetes system that is well suited for local development and testing of WESkit. See [here](https://minikube.sigs.k8s.io/docs/start/) for installation instructions. Note that the following assumes, that Minikube runs with a rooted Docker daemon.

Minikube is only one option. You can certainly also use other Kubernetes deployments. Minikube is our current deployment system, and consequently much of the documentation below relates to Minikube.

Note that you should increase some OS limits for Kubernetes:

```bash
$ cat - <<EOF > /etc/sysctl.d/10-minikube.conf 
fs.inotify.max_queued_events = 16384
fs.inotify.max_user_instances = 512
fs.inotify.max_user_watches = 262144
user.max_inotify_instances = 512
user.max_inotify_watches = 262144
EOF
```

### Network Setup

By default, with `minikube start`, Minikube uses the subnet 192.168.49.0/24 for the first cluster. Your control-plane, the cluster IP, will automatically be located at 192.168.49.2.

In the following we assume the internal gateway IP is 192.168.49.1 (`$internalGatewayIP`), and the cluster IP is 192.168.49.2 (`$clusterIP`; Minikube's default for the first cluster). This means, if you want to contact the cluster, you contact the `$clusterIP`. If you want to contact the host from within the cluster, you can contact the `$intenalGatewayIP` (you can also contact the external IP of your host, though).

If you want to find out these IPs for an existing Minikube cluster you have a number of options

 * Get the cluster IP
   ```bash
   minikube ip
   minikube ssh ip r \
      | grep "dev eth0 proto" \
      | cut -d ' ' -f 9
   minikube profile list -o json \
      | jq .valid[0].Config.Nodes[0].IP
   kubectl describe nodes \
      | grep InternalIP \
      | cut -f 5 -d ' '
   kubectl cluster-info
   ```
 * Get the internal gateway IP
   ```bash
   minikube ssh ip r | head -n 1 | cut -f 3 -d ' '
   docker network inspect minikube | jq .[0].IPAM.Config[0].Gateway
   ```

#### Static Network

If Minikube's default does not work for you, you may manually create a docker network and connect the Minikube-managed Kubernetes cluster to that network. See [here](https://github.com/kubernetes/minikube/issues/12315#issuecomment-1063813918). Similarly, within that subnet you can also set the IP of control-plane node [statically](https://minikube.sigs.k8s.io/docs/tutorials/static_ip/). The control-plane nodes' IP is the IP used to access the cluster from the host.

A setup that fixes all these addresses to specific values would be

```bash
# First create your network, e.g. for Docker as driver.
docker network create \
  --driver=bridge \
  --subnet=192.168.60.0/24 \
  --gateway=192.168.60.1 \
  my-minikube-network
# Then start Minikube on this network and assign the 
minikube start \
  --driver docker \
  --network my-minikube-network \
  --static-ip 192.168.60.2
```

This will create a network in Docker and let it be used by Minikube. The internal gateway (~ host's IP from within that network) will be 192.168.60.1, and the cluster IP (control-plane node) will be 192.168.60.2.


#### Proxy

Please see the [documentation](https://minikube.sigs.k8s.io/docs/handbook/vpn_and_proxy/) if you are running Minikube behind a proxy and/or VPN. In particular, make sure you have the CIDR address ranges of the K8s cluster node, i.e., the subnet configured by Minikube (e.g. 192.168.49.0/24) or of you own static bridged network (see above) -- in your `no_proxy/NO_PROXY` variable. E.g.

```bash
export NO_PROXY="$NO_PROXY,10.0.0.0/8,192.168.0.0/16"
export no_proxy="$NO_PROXY"
```

### Mounts

The current configuration assumes that the internal and external user and group IDs (not names) correspond, either directly (like in the local storage mounts) or implemented via squashing or remapping (for NFS mounts).

#### Local storage mounts

If you intend to use the [local storage class](https://kubernetes.io/docs/concepts/storage/volumes/#local) you need to make sure to keep a port open for the 9P mounts. 

> NOTE We noticed that many Linux distributions do not contain kernel modules for 9P by default. In principle, it is possible to fall back to "hostPath" storage, although the [Kubernetes documentation](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath) advises against it for security reasons. Note, that the Helm chart is not prepared for "hostPath" storage. The required changes are probably small. Please file a feature request, if you need this. The permission issues described below will also apply to "hostPath" storage. 

The first thing to keep in mind with the 9P mounts from Minikube is that the user and group IDs in the containers and the host system need to be identical.

  * The internal IDs in the bitnami charts are UID 1001 and GID 1001.
  * The WESkit containers the UID and GID are hardcoded in the [default Dockerfile](https://gitlab.com/one-touch-pipeline/weskit/api/-/blob/master/Dockerfile#L24) and both 35671.

Thus, you need to create these users and groups on the host and set the permissions for the mounted volumes accordingly. Here an example with all directories in a single volume and two groups "bitnami" (1001) and "weskit" (35671) that have the development user as only member. The example also includes the source code for development.

```bash
developmentUser="The name of your developmentUser"
sudo groupadd -g 35671 weskit
sudo usermod -a -G weskit $developmentUser
sudo groupadd -g 1001 bitnami
sudo usermod -a -G bitnami $developmentUser
```

Then as user

```bash
mountPoint="/home/$USER/localVolumes"
mkdir "$mountPoint"
cd "$mountPoint"
# Pull api-repository into api/ directory
git clone git@gitlab.com:one-touch-pipeline/weskit/api.git

# For the WESkit containers
mkdir data/
chgrp weskit data/
chmod 0770 data/

# For the Bitnami containers
mkdir redis/ mongodb/
chgrp bitnami redis/ mongodb/
chmod 0770 redis/ mongodb/ 
```

Furthermore, the 9P filesystem used by Minikube needs network access to the cluster. Therefore

```bash
ufw allow in from $clusterIP to $internalGatewayIP port 38069 proto tcp 
minikube start \
  --driver docker \
  --container-runtime docker \
  --mount=true \ 
  --mount-port=38069 \
  --mount-string="/home/$USER/localVolumes/:/localVolumes"
```

> WARNING: Note that this uses the "docker" driver. By default, on your system another driver may be used. The following instructions may not apply if you use another driver. In particular, we found that using a stronger separation of the host and minikube, e.g., by using the kvm2 driver, makes the deployment much more complicated. 

This will mount your directories at `/localVolumes` in the Minikube cluster. You can also add mounts to a running cluster with

```bash
minikube mount \
  --port=38069 \
  /home/$USER/localVolumes/:/localVolumes
````

You can validate the mount by SSHing into the cluster

```bash
$ minikube ssh
> ls /localVolumes
redis mongodb data
```

> Note: If something goes wrong, it may help to run `minikube delete` to start over with a new minikube cluster.

Finally, follow the instructions in the `values.yaml` to configure your persistent volumes. The paths that you need to list in the `values.yaml` are the paths internal in the cluster, e.g., in this example here `/localVolumes`.

#### NFS mounts

You can also configure [nfs](https://kubernetes.io/docs/concepts/storage/volumes/#nfs) as `storageClass` in the `values.yaml`, but then you need a running NFS server. Setup and configuration of an NFS server it out of scope of this document. Some hints, though:

  * For development, it is maybe easier to export the volumes with `all_squash,anonuid=$weskitUID,anongid=$weskitGID`. This will map all files to user the user and group that you use also for WESkit. We create a user weskit:weskit on the development host and add the development user to the group "weskit".
  * You should also export the shares to "localhost" (to test the mount without Minikube), the `$clusterIP`, and maybe the external address of the host on which you run Minikube.
  * Do not forget to open the correct ports in the firewall.
  * In particular for production, there is the option to use NFSv4 with id mapping. This provides an alternative solution to the problems of mismatching user and group IDs in the different WESkit service containers and the host system.

If you have verified that you can mount the shares from the localhost, you can test the mount from the Minikube by logging in to Minikube with

```bash
$ minikube ssh 
> sudo su -
> mount -v -t nfs -o vers=4 $clusterIP:/export/ /mnt/
```

Then in the `values.yaml` set the `storageClass` fields of the desired volumes to "nfs" and configure the remaining field:
 * `host`: Name or IP of the NFS server. For a development deployment in which the NFS shares are on the developer PC you may use the `hostname` of the developer machine, or the `$internalGatewayIP`.
 * `path`: Exported path.
 * `size`: Depends on the storage provisioner in Kubernetes. If you create shares manually, this is probably not needed.

### Configuring SSH-based Executors

WESkit can contact servers for executing the workflow engines remotely. For this, the engine executor has to be configured. For instance, here an example of a `executor` section from the Helm `values.yaml` that assumes the following setup

  * The `SshExecutor` should be used, i.e. workflows should be run directly on a remote host that is contacted via SSH.
  * The host is the development host on which the cluster is running, so we use the `$intenalGatewayIP`, e.g. 192.168.49.1.
  * The username for login is "developer"
  * You need to provide a (preferably non-empty) keyfile passphrase.
  * The data directory (`WESKIT_DATA`) and the workflows directory (`WESKIT_WORKFLOWS`) on the remote side (i.e. on the SSH-server, configured via `weskit.executor.remote.dataDir` and `weskit.executor.remote.workflowsDir`) are the same as in the deployed container (`weskit.dataDir` and `weskit.workflowsDir`).

Note that if the executor type is set to any of the SSH-based executors ("ssh", "ssh_lsf", "ssh_slurm") then the Helm chart expects a private keyfile and as SSH `knownhosts` file (`ssh/cluster` and `ssh/knownhosts`), and the host/IP column in the `knownhosts` file must match the hostname/IP used in the `host` field in the `values.yaml` here.

```yaml
weskit:
  dataDir: /data/data
  workflowsDir: /weskit/tests
  executor:
    type: ssh
    remote:
      # The cluster's submission node (name or IP)
      host: 192.168.49.1
      # # SSH port, defaults to 22
      # port: 22
      username: developer
      passphrase: "this is a keyfile passphrase"
      # dataDir:
      # workflowsDir:
      # singularityContainerDir: /containers/singularity
```

The SSH-configuration for the "ssh_lsf" "ssh_slurm" executor types is analogous.

Finally, for this to work, the executed workflow engine needs to be available on the remote side. The best (and future-proof) option is to execute the workflow engine in a Singularity container. To this aim, you need to define the workflow engine version in the `weskit.yaml`, and set the `singularityContainerDir` to a path in the execution infrastructure (e.g., your cluster).

## Deployment Commands

After initial checkout you should first do a `helm dependencies update`, to retrieve the required external charts.

There need to be TLS certificate files (`weskit.key`, `weskit.crt`, `weskit.pem`) in the `certs` directory. You can generate certificates for development with `generateDevelopmentCerts.sh certs/`. Dependent on your configuration you can also provide a hostname and host IP as additional parameters, e.g. `generateDevelopmentCerts.sh certs/ weskit $clusterIP`.

1. [Generate](https://helm.sh/docs/intro/using_helm/#customizing-the-chart-before-installing) a configuration from the default configuration
   ```bash
   helm show values helm-deployment/ > your-values.yaml
   ```
2. Configure by copying and editing `your-values.yaml`.
3. Deploy
   ```bash
   helm install -f your-values.yaml weskit-devel-1 helm-deployment/ 
   ```

## Building a Local Image and using it in Minikube

In case you built an image to adapt the UID/GID in the container to match your outer functional user UID/GID, you may want to use the image in the Minikube-based deployment.

You can copy the image from your hosts docker daemon into Minikube with a command similar to the following:

```bash
minikube image load registry.gitlab.com/one-touch-pipeline/weskit/api:myAdaptedContainer-1.0
```

You may then also want to adapt the name of image in your helm charts accordingly.

## Production Deployment

### Submit workflows with SSH & share data _via_ NFS filesystem without Kerberos

In this setting, WESkit submits its workflows into a compute cluster via SSH (issuing submission commands on the command-line) and accesses the data via NFS.

1. Set up an SSH-key for remote login
   ```bash
   ssh-keygen -t ed25519 -f ssh/cluster
   ssh-copy-id -i ssh/cluster $remoteUser@$submissionHost
   # Validate key
   ssh -i ssh/cluster $remoteUser@submissionHost echo Hello
   # should print "Hello"
   ```
2. Configure the SSH-key in the `values.yaml`. For the sake of the example, the NFS server is the local machine (think of a developer deployment). In the `values.yaml`
   ```yaml
   weskitData:
     # Allowed storageClass values: local, nfs, maybe hostPath (not tested)
     storageClass: local
     size: &weskitStorageSize 100Mi
     local:
       path: /localVolumes/data
     nfs:
       # The export on the local machine. We assume the directory contains both the data and 
       # the workflows in the data/ and workflows/ subdirectories.
       path: /export/data
       # The NFS server as seen from the WESkit deployment.
       server: &nfsHostIp 192.168.49.1
       kerberos:
         enabled: false
   weskitWorkflows:
    # Allowed storageClass values: local, nfs, maybe hostPath (not tested)
    storageClass: local
    size: 100Mi
    local:
      # The mount point on the Kubernetes hosts, *not* the one in the containers.
      path: /localVolumes/workflows
    nfs:
      path: /export/workflows
      server: *nfsHostIp
      kerberos:
        enabled: false
   ```
3. Configure the executor in the `weskit` section of the `values.yaml`:
    ```yaml
    weskit:
      api:
        # Please adapt the container version.
        image: &weskitImage registry.gitlab.com/one-touch-pipeline/weskit/api:dev-4.1.0
      service:
        # Whether to expose a port via the nodeport mechanism.
        deployNodePort: true
        nodePort: 30132
      worker:
        image: *weskitImage
        pool: gevent
        concurrency: 1
      ssl:
        enabled: true
      dataDir: /data
      workflowsDir: /workflows
      executor:
        type: ssh_lsf
        remote:
          # Also here the submission host is the localhost as seen from the WESkit deployment.
          host: 192.168.49.1
          username: $remoteUser
          passphrase: "your secret passphrase"
          # These are the directories with the data and the workflows as seen when logged in on the cluster.
          # We assume that on the cluster the data and workflows are installed in the following directories.
          # These are subdirectories of the mounted share.
          dataDir: "/export/data"
          workflowsDir: "/export/workflows"
    ```

### Kerberos

TBD

### Kernel Settings

Both MongoDB and [Redis](https://www.techandme.se/performance-tips-for-redis-cache-server/) need some kernel parameters to be set, in order to ensure performance and failure safety for persistence.

```bash
$ cat - <<EOF > /etc/sysctl.d/50-weskit.yaml
vm.overcommit_memory = 1
net.core.somaxconn = 65535
EOF
$ cat - <<EOF > /etc/rc.d/rc.local
echo never > /sys/kernel/mm/transparent_hugepage/enabled
echo never > /sys/kernel/mm/transparent_hugepage/defrag
EOF
```
