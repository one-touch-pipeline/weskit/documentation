# Workflow-Installation

The following describes the installation and configuration of workflows for the use case with a static installations of workflows in the WESkit infrastructure.

# Environments

WESkit executes the workflow engines, like Snakemake and Nextflow, natively or in containers.
Thus, WESkit keeps a strong separation between its own code and the code of the workflow engines.
A detailed overview is given in the [architecture documentation](../technology/architecture.md).

We refer to the environment of the workflow engine control process as "workflow engine environment".
By contrast, we call the environment of the tasks that the engine submits as "task environment", sometimes prefixed with "workload", because most workflows are (and should be) implemented in such a way that the resource-heavy workload is done in these tasks.

The default configuration of where the workflow engine submits the workload tasks is completely left to the workflow engine.
WESkit may provide some REST parameters that modify this configuration, though, but that is a matter of configuration and of fitting and integrating the workflow engine, or even specific workflow, into WESkit. 

## Workflow engine environment

The place where your workflow engine processes are being run depends on the `executor` configuration of your WESkit instance.
The following `executor.type` values are available:

  * `local`
    * Run the engine directly in the Celery worker container in the WESkit infrastructure. 
    * This mode of execution is mostly for testing and provides little flexibility with respect to memory and CPU resources used for the workflow engine.
  * `ssh`
    * Run the engine remotely via an SSH connect. The Celery workers can be very small because it just manages the SSH connection and command pre- and post-processing. 
    * The workflow engine needs to be installed on the remote host.
    * Note that currently asynchronous engine execution is not implemented. Thus, with SSH-based execution the SSH connection will have to be kept open as long as the workflow engine runs. 
  * `ssh_lsf` and `ssh_slurm`
    * Run the engine in a cluster job. The submission host is accessed via an SSH connection.
    * In principle, this implements asynchronous workflow engine execution in the cluster, e.g. via `bsub`, but the current implementation runs `bwait` synchronously.
  * `local_lsf` and `local_slurm`
    * These are for local submission into a cluster from the worker container.
    * Basically, this requires the LSF or SLURM cluster commands to be configured in and executable from the worker container. This is possible, in principle (see [here](https://gitlab.com/one-touch-pipeline/weskit/api/-/issues/74#note_663811012)), but not simple and may have security implications. 

In all cases, you can define a script that is executed before the workflow (with `source`) and can be used to set up the environment, e.g. to load a workflow engine specific Conda environment or cluster modules.

  * The engine environment setup script can be defined via the `engine-environment` workflow engine parameter. So, in principle, it can be defined via API calls.
    * The path needs to be an absolute one, or a relative to the run-directory in the execution environment.
    * Both, relative and absolute paths, are currently accepted also via the API.
    > IMPORTANT: We strongly discourage exposing the `engine-environment` to the API, because it is a security risk.
  * If an engine-environment is defined, on the execution host of the workflow engine the command `source '$envPath'` will be executed before the workflow engine call.
  * If you want to make decisions in the setup script based on the workflow engine version or workflow path, then you can retrieve these from the following environment variables:
    * `WESKIT_WORKFLOW_PATH`: Path to the executed workflow file, e.g. `Snakefile` or `main.nf`.
    * `WESKIT_WORKFLOW_ENGINE`: A value composed of the configured name and version of the workflow engine that will be run. Specifically, `$workflowEngine + '=' + workflowVersion`

### Examples: Relative environment path

If you want workflow-specific environments, your runs are executed in the UUID directory `WESKIT_DATA/uuidPrefix/fullUuid/`, and your environment setup script is `WESKIT_DATA/workflows/worflow1/environment.sh`, then a correct relative path is `../../workflows/workflow1/environment.sh`.

### Example: Dispatch on engine and workflow

If you have central environment-script, it may locate an engine- and workflow-specific environment by composing the path to a workflow- and engine-specific environment file that is located in the workflow installation directory with the name of the workflow engine: 

```bash
# Assumes, that the primary workflow file is directly in the workflow path
workflowDir="$(dirname "$WESKIT_WORKFLOW_PATH)")"
source "$workflowDir/$WESKIT_WORKFLOW_ENGINE"
```

The workflow engine names are then e.g. "SMK=7.30.2".

## Workload job environment(s)

The job environment of the workload jobs submitted by the workflow engine is completely configured by the workflow engine, e.g. by Nextflow's [profiles](https://www.nextflow.io/docs/latest/config.html#config-profiles) or by Snakemake's `env` rule-attributes and in the YAML files in the `envs/` directory.
The available types of job-environments -- Conda, Docker, Singularity, etc. -- depend on the configuration of your job execution infrastructure. 

Usually, it is a good idea to install workflows with such a configuration, that the job environments are installed once in a central place, e.g. some `cache/` subdirectory of the workflow installation.

## Workflow Deployment

Configure the `WESKIT_WORKFLOWS` environment variable to point to the absolute base-path of your centrally installed workflows, i.e. where it is mounted in the WESkit container.
All workflows should be installed in subdirectories of this directory.
This is also the place where workflows are automatically installed, if TRS support is active.

WESkit only accepts and returns relative paths via the API. In this example, a REST client should refer to a workflow via its path relative to the `$WESKIT_WORKFLOWS` directory.
So, for instance, `file:AwfulWorkflow-1.3.0/main.nf` would be a valid workflow path if the path is `$WESKIT_WORKFLOWS/AwfulWorkflow-1.3.0/main.nf`.

### Nextflow Example

We use the [nf-seq-qc workflow](https://gitlab.com/one-touch-pipeline/workflows/nf-seq-qc) as an example, and install it such that it runs locally in the worker containers. Let's first install the workflow:

1. Locate your `workflows/` directory and change into it:
   ```bash
   cd workflows/
   ```
2. Get your workflow
   ```bash
   git clone https://gitlab.com/one-touch-pipeline/workflows/nf-seq-qc.git
   ```
3. Ensure the Conda cache directory is writable to the user in the container. For this workflow the "conda" profile defines the Conda cache dir to be "${projectDir}/cache/conda" (which is generally advisable). Therefore, on the host, we make the file writable to everybody:
   ```bash
   mkdir -p nf-seq-qc/cache/conda
   chmod o+rwX -R nf-seq-qc/cache/conda
   ```
4. Now, start the container with the user that will also be used in the deployment, and mount the workflow-directory as volume:
   ```bash
   docker run -u weskit \
     --env HTTP_PROXY=$HTTP_PROXY \
     --env http_proxy=$HTTP_PROXY \
     -it \
     --rm \
     -v $PWD:$workflowsInternalPath:rw \
     registry.gitlab.com/one-touch-pipeline/weskit/api:latest \
     /bin/bash
   ```
   Note that the `workflowsInternalPath` must be the directory used as value for `WESKIT_WORKFLOWS`. For the [test-deployment](https://gitlab.com/one-touch-pipeline/weskit/deployment/-/blob/master/static/weskit_stack_base.yaml#L36) this is `/weskit/workflows/tests/`.
5. Within the container, activate the "weskit" environment, because it contains `Nextflow`. Then build the workflow's library.
   ```bash
   conda activate weskit
   cd $workflowsInternalPath/nf-seq-qc
   ./gradlew build
   ```
6. Now it is time to create the task environment(s). For this workflow, the easiest way is to run the integration test that runs the (one and only) task:
   ```bash
   mkdir test-results && \
     tests/runIntegrationTests.sh conda test-results && \
     rm -r test-results
   ```
   This will install the Conda job environment into the `cache/conda` directory in the workflow directory.
   > WARNING: Conda requires that the path at installation time of an environment is the same as when the environment is used. You may work around this limitation by packing an environment from an installation at some place and moving it to the target path with [conda pack](https://conda.github.io/conda-pack/).
7. Exit the container and fix the permissions in the workflow
   ```bash
   exit   # from the container
   chmod o=rX -R nf-seq-qc/cache/conda
   ```
   Note that, unless you use the same user and group ID inside and outside the container, without root-rights the permissions and ownership of the environment files may not be easily changed from outside the container.
8. By default, Nextflow uses the "standard" profile, but for this workflow, a "standard" profile is not defined. You can define one by editing the `nextflow.config` in the workflow directory.

We also want to validate, that the workflow actually works with this deployment. Therefore,

1. Let's copy the test data from the workflow into your `data/` directory. E.g. if `data/` is a sibling directory of `workflows/`:
   ```bash
   mkdir ../../data/test-input/
   cp \
    tests/seqs/run1_gerald_D1VCPACXX_1_R1.sorted.fastq.tar.bz2 \
    tests/seqs/run1_gerald_D1VCPACXX_1_R1.sorted.fastq.gz \
    ../../data/test-input/
   ```
1. Run you workflow:
   ```bash
   run_id=$(curl \
    --ipv4 \
    --cacert /path/to/certs/weskit.crt \
    -X POST  https://localhost:443/ga4gh/wes/v1/runs \
    --header 'Content-Type: multipart/form-data' \
    --header 'Accept: application/json' \
    -F workflow_params='{ "input": "/data/test-data/run1_gerald_D1VCPACXX_1_R1.sorted.fastq.tar.bz2,/data/test-data/run1_gerald_D1VCPACXX_1_R1.sorted.fastq.gz", "outputDir": "." }' \
    -F workflow_type="NFL" \
    -F workflow_type_version="23.04.1" \
    -F workflow_url="nf-seq-qc/main.nf" \
     | jq -r .run_id)
   ```
   Note that WESkit has no information about the semantics of the parameters. Therefore, you have to provide the paths to the input files as absolute paths *in the container*.
1. List all running workflows
   ```bash
   curl \
    --ipv4 \
    --cacert /path/to/certs/weskit.crt \
    https://localhost:443/ga4gh/wes/v1/runs
   ```
1. Check the run status.
   ```bash
   curl \
    --ipv4 \
    --cacert /path/to/certs/weskit.crt \
    https://localhost:443/ga4gh/wes/v1/runs/$run_id
   ```
1. Go to the result directory and look at the results.
   ```bash
   workdir=$(\
     curl \
     --ipv4 \
     --cacert /path/to/certs/weskit.crt \
     https://localhost:443/ga4gh/wes/v1/runs/$run_id \
     | jq -r .run_log.workdir)
   cd ../data/$workdir
   ls -al ./
   ```

## Nextflow

Note that there are two types of configurations in Nextflow.

The first type are "workflow parameters", which are set via `-params-file` or double-dash command-line parameters.

These end up in the `params` dictionary that you can access from the `.nf` workflow file and in the `nextflow.config` file.

WESkit uses the `-params-file` parameter to set workflow parameters. Specifically, WESkit creates a `config.yaml` from the `workflow_params` option in the REST call to submit new workflow runs.

The second type are configurations that you will find in the `nextflow.config`, including things like executor configurations (LSF, SLURM, etc.), etc.

Nextflow has built-in support for cascaded configurations, which is well described in its [documentation](https://www.nextflow.io/docs/latest/config.html#configuration-file).

Configurations can be put at different places and then apply to all Nextflow runs, the Nextflow runs of a specific workflow, or only a specific Nextflow run.

Like described above, Nextflow also allows to refer in your `nextflow.config` to workflow parameters with `params` dictionary -- independent of where you position this configuration file.

For instance, if you want to set the accounting information associated with the cluster jobs that Nextflow sends to your LSF cluster, you may do this as follows:

  1. Add the `clusterOptions` to your LSF profile:
     ```groovy
     params {
        accountingName = "defaultAccount"
     }
     profiles {
        tiny {
          cpus = 1
          memory = 1.GB
        }
        lsf {
            process {
                executor = 'lsf'
                clusterOptions = "-P '$params.accountingName'"
            }
        }
     }
     ```
     Note how the `params` dictionary is referenced and the `accountingName` parameter extracted (this is a Groovy string). You may have to set the `params.accountingName` in a `params` section of the configuration. The value can be changed with values provided by the `-params-file` option.
  2. If the workflow is well implemented, it will check the validity of the keys in the `params` dictionary. Thus, a good workflow will not accept any custom key like `accountingName` among its parameters. But this also means that you may have to modify your workflow to accept an `accountingName` workflow parameter.
  3. The WESkit client has to use the `accountingName` in its REST call (each field as form-data of type string)
     ```
     workflow_url="file:workflows/AwfulWorkflow-1.3.0/main.nf"
     workflow_params='{
       "accountingName": "my-project-will-pay"
     }
     workflow_type="NFL"
     workflow_type_version="23.04.1"
     tags='{
        "user_id": "itsMe",
        "run_dir": "file:projects/name/of/projectX/and/workdir"
     }'
     workflow_engine_parameters='[]'
     ```
     The "run_dir" is only allowed, if `require_workdir_tag` is set in the `weskit.yaml`.

An alternative implementation might circumvent the problem with the modification of the workflow by using plain environment variables. But ...

> NOTE: Currently setting of environment variables for individual runs via the REST-API is not possible with WESkit.

> Part of the problem is that the API specification is not clear about how to deal with workflow-engine parameters like this.

Now the question is of course, where to put the `nextflow.config`. One extreme approach would be to not define any default configurations and only use the client-uploaded `nextflow.config`.

However, if you want to define default configurations, you can do so for defaults of various types. Please refer to the [Nextflow documentation](https://www.nextflow.io/docs/latest/config.html#configuration-file) to learn about the evaluation order of these configurations.

### Global Nextflow Configuration

You want that your configuration affects all workflow runs done by WESkit.

E.g. all Nextflow runs should use the LSF executor and the setting of an `accountingName` should be mandatory.

Nextflow reads in the `$HOME/.nextflow/config`.

So one way to implement this would be to ensure this file exists in the `$HOME` of the workflow-executing user in the Celery worker container.

The default config would be in `/home/weskit/.nextflow/config` (that is the `$HOME` according to the [Dockerfile](https://gitlab.com/one-touch-pipeline/weskit/api/-/blob/master/Dockerfile)), but you could also set the `NXF_HOME` variable in your container (see [here](https://www.nextflow.io/docs/latest/config.html?highlight=nxf_#environment-variables)) and instead use a mounted share, e.g. in `$WESKIT/workflows/.nextflow/config`.

This would give you more flexibility.

### Nextflow-Version Configuration

You may want to use different configuration files for different versions of Nextflow.

This can be achieved by configuring separate `NXF_HOME` [environment variables](https://www.nextflow.io/docs/latest/config.html?highlight=nxf_#environment-variables) for each version of Nextflow.
This can be done in the `weskit.yaml` via the workflow engine parameters.

### Workflow-Specific Configuration

Workflows may have workflow-specific configurations that you have to configure for all runs of that workflow.

Nextflow reads in the `nextflow.config` in the workflow's installation directory, which is accessibly from within your `nextflow.config` via the `projectDir` variable.

For instance, for a workflow installed at `file:workflows/AwfulWorkflow-1.3.0/main.nf`, `workflows/AwfulWorkflow-1.3.0/nextflow.config` will be the workflow-specific `nextflow.config`.

Just create such a file there, to configure all runs of that workflow.

### Run-Specific Configuration

You want to allow the WESkit client to modify the configurations in the `nextflow.config`.

The way to achieve this, is that you upload a `nextflow.config` via the REST call using a `workflow_attachment`.

## Snakemake

The installation procedure is basically very similar to the one of Nextflow workflows. Some more hints:

* You can install the workflow's Conda environments with `snakemake --use-conda --conda-create-envs-only`
