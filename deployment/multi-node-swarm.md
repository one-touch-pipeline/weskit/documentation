# Multi-Node Swarm

This works like expected. Here some documentation links and usefull commands:

* [docker swarm init](https://docs.docker.com/engine/reference/commandline/swarm_init/)
* [docker swarm join](https://docs.docker.com/engine/reference/commandline/swarm_join/)
* [docker swarm join-token](https://docs.docker.com/engine/reference/commandline/swarm_join-token/) to retrieve join tokens for manager or workers
* [docker node ls](https://docs.docker.com/engine/reference/commandline/node_ls/)

However, the firewall needs to be configured accordingly (see [here](https://docs.docker.com/network/overlay/#create-an-overlay-network) and [here](https://www.digitalocean.com/community/tutorials/how-to-configure-the-linux-firewall-for-docker-swarm-on-ubuntu-16-04))

* port 2376, tcp, in/out: secure Docker client communication
* port 2377, tcp, in/out: cluster management communications
* port 7946, tcp/udp, in/out: communication among nodes
* port 4789, udp, in/out: overlay network traffic

