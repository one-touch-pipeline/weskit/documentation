# Proxy Configuration

  1. Create a `/etc/profile.d/proxies.sh`
     ```bash
     cat - <<EOF > /etc/profile.d/proxies.sh
     export http_proxy=http:/your.http.proxy:port
     export HTTP_PROXY=$http_proxy
     # The following may also be http (w/o "s") for some proxies. Try this if you get errors from pip.
     export https_proxy=https:/your.http.proxy:port
     export HTTPS_PROXY=$https_proxy
     # The following to prevent that localhost-requests go via the proxy.
     # You may also want to add your local domains here.
     export no_proxy="localhost,127.0.0.1"
     export NO_PROXY="$no_proxy"
     EOF
     chmod 0755 /etc/profile.d/proxies.sh
     ```
     After that source the file into your environment.
  2. Create a `.docker/config.json`
     ```bash
     mkdir -p ~/.docker
     cat - <<EOF >> ~/.docker/config.json
     {
         "proxies": {
         "default": {
           "httpProxy": "$HTTP_PROXY",
           "httpsProxy": "$HTTPS_PROXY",
           "noProxy": "$NO_PROXY"
         }
       }
     }
     EOF
     ```
  3. Add the following to your `~/.gitconfig`:
      ```bash
      cat - <<EOF >> ~/.gitconfig
      [http]
      proxy = $HTTPS_PROXY
      noproxy = localhost,127.0.0.1
      EOF
      ```
  4. If you want to test the service with cURL you can create `~/.curlrc` as follows:
     ```bash
     cat - <<EOF >> ~/.curlrc
     proxy=$HTTP_PROXY
     noproxy=$NO_PROXY
     EOF
     ```
