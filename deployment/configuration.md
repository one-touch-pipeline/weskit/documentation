# Configuration

## WESkit and components

Template configuration files are included in the `configs/` directory of the deployment repository. There you find the following files

* `devel-log-config.yaml` and `log-config.yaml` configure Python's "logging" module.
* `redis.conf`: Configuration used by the [Redis](https://redis.io/documentation) container
* `weskit.yaml`: Configuration of the WESkit GA4GH-compliant REST server.
* `mysql.conf`: ??
* Keycloak initialization (DB)?

## Deployment

The deployment script uses two [Docker compose files](https://docs.docker.com/compose/compose-file/).

1. The base-compose file contains all services required to run the stack on a standard machine. This file should not be edited by the users.

2. The user-compose file adapts the standard settings to the user requirements. The user-compose file can overwrite and adapt any settings from the base-compose file. Using the user-compose file is optional and can be set by using the `--compose` argument at the start:

```bash
python weskit_stack.py start --compose $USER_COMPOSE_FILE
```

### Base-compose file

The [base compose file](https://gitlab.com/one-touch-pipeline/weskit/deployment/-/blob/master/weskit_stack_base.yaml) contains standard settings which can be adapted and be overwritten by the user-compose file. The base stack consists of five different services.

* **rest**: the python flask app that receives the HTTP requests and communicates with the database and the celery worker. This app is developed by the weskit team and available in a weskit image.
* **database**: the MongoDB database stores all run information required by the weskit app.
* **result_broker**: Redis is used as a message queue between the flask app and the celery worker.
* **worker**: The celery workers execute the workflows and watch their status.
* **traefik**: Traefik provides TLS decryption and forwards incoming traffic into the rest service.

### User-compose file

To make customization as simple as possible for new users, the user-compose file offers only limited customizations options per default. Nevertheless, advanced user-compose files can be used by setting the `--expert_mode` flag at the start.

In user_mode (default mode) users can set:

* custom weskit/api images for the `rest` and the `worker` services.
* custom volumes for the `rest` and the `worker` services.

### Custom workflows

You can install your workflows in `test/workflows/`. You can get more information on workflow installation and configuration [here](https://gitlab.com/one-touch-pipeline/weskit/documentation/-/blob/master/technology/workflow_installation.md)

## Containerized workflow execution

### Prerequisites

**Singularity/Apptainer Installation:** Ensure that [Singularity/Apptainer](https://apptainer.org/documentation/) is installed and configured on the cluster.

**Engine Container:** The required container image for the workflow’s execution engine must be pre-installed/put in a distinct folder.

**Cluster Scheduling System:** If a scheduling system such as Slurm is used, ensure configurations are known and ready to be provided to the WESkit configuration.

### Parameters for Workflow Execution

`singularity_containers_dir`

The `singularity_containers_dir` parameter specifies the directory where the Singularity container resides. This enables the workflow to locate and execute within the appropriate container environment. Set it to `None` if you do not want to execute your workflow within the conatiner.

`container_mounts`

The `container_mounts` parameter allows additional directory mounts required for workflow execution. These mounts provide access to essential files and directories.

These include:

* Data directories containing input files for the workflow.

* Directories containing configuration files.

### Scheduling System-Specific Mounts

Workflows executed under a scheduling system such as Slurm may require access to specific system directories.

For example:

* /usr/lib64
* /usr/bin
* /etc/passwd
* /etc/group
* /opt/slurm/etc
* /var/spool/slurmd
* /var/run/munge
* /usr/lib64/slurm

These directories should be added to the container_mounts parameter, ensuring they are accessible (read-only) within the container.

#### Mount Permissions

When specifying mounts, ensure that the access mode is set appropriately. Valid permissions are `ro` (read-only) and `rw` (read-write).

For directories required for scheduling systems or other read-only resources, the mode should be set to `ro` (read-only).
