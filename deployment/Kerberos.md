# Mounting central Kerberos-protected storage

## ... for a single-user installation

The following describes how to mirror the host's Kerberos configuration into the containers. In this setting, the TGT is managed at the host level, which means that also ticket renewal needs to be done at this level.

* The weskit images already contain the `kinit` tool.
* The TGT can be mounted into the container.
  * The procedure is
    1. Get TGT on host for the user ID that is going to be used in the containers: `kinit -r7d -c /etc/krb5cc_$UID`
    2. Mount the storage at host level.
    3. Validate that the storage can be accessed with the user.
    4. Mount the host kerberos config into the container. The following needs to be added to the stack/compose file:
       ```yaml
       service:
         rest:
           volumes:
            - /etc/krb5.conf:/etc/krb5.conf:ro
            - /etc/krb5.conf.d:/etc/krb5.conf.d:ro
         worker:
           volumes:
            - /etc/krb5.conf:/etc/krb5.conf:ro
            - /etc/krb5.conf.d:/etc/krb5.conf.d:ro

       ```
    5. Mount TGT into containers using the same name as outside the container. Again, the changes for the stack/compose file:
       ```yaml
       secrets:
         kerberos_tgt:
           file: {{ kerberos.tgt_file }}
       ...
       service:
          rest:
            secrets:
              - source: kerberos_tgt
                target: {{ kerberos.tgt_file }}
          worker:
            secrets:
              - source: kerberos_tgt
                target: {{ kerberos.tgt_file }}
       ```
    6. Start the stack.
    7. Log in to the container and validate that you can access the storage
       ```bash
       docker exec -it \
         --user weskit \
         $workerContainerId \
         /bin/bash
       ```
    8. Then try to access the storage at the point it is mounted into the container's filesystem.
  
### Security implications

Use this on production-only systems preferably with production only NFS accounts. The TGT will be readable for all users having access to the system, even if they do not have root access. 