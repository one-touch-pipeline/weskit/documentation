# Requirements

The following is a list of requirements for running WESkit.

* Services
  * See [WESkit Kubernetes Deployment](helm.md)
  * Docker for building or converting the container
* LSF executor
  * The current LSF versions should work.
* SLURM executor
  * A recent SLURM version should work.
* Kubernetes executor
* SSH executor (also needed for SSH-based execution via LSF or SLURM)
  * SFTP support (for remote file access)
  * Coreutils
  * Bash
