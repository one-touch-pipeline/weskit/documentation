# WESkit Demo using Slurm

This guide provides step-by-step instructions to configure an existing WESkit installation to use Slurm as the execution backend. 

> NOTE: The guide was tested for ``Ubuntu-24.04``.

## Prerequisites

### On your machine
- [Docker](https://docs.docker.com/engine/install/ubuntu/)
- Conda (e.g. [Miniforge](https://github.com/conda-forge/miniforge))

### In your cluster
- Slurm
- Slurm accounting enabled (`sacct` is working)
- Your workflows can be executed on the Slurm worker nodes (e.g. Snakemake/Nextflow is installed)

> NOTE: If you have access to the de.NBI cloud, you can use [BiBiGrid](https://github.com/deNBI/bibigrid_clum?tab=readme-ov-file#premade-template) to set up a Slurm test-cluster.


## Step 1: Slurm configuration

### Shared Data

Make sure to have a shared directory that can be accessed by all Slurm nodes. This directory must contain:

1. The ``data`` directory for running the workflows. It will contain the work directories for each workflow.
2. The ``workflow`` directory, containing the workflow files. Here you have to store e.g. the actual Snakefile/.nf files, or the config files for your workflows.

### Setup Workflow Environment

WESkit will run your workflows on the Slurm nodes. Therefore they must be set up beforehand. This may include installing Conda, Snakemake, Nextflow, etc. Also the data used by the workflows must be accessible.


## Step 2: WESkit configuration

### Install WESkit

To install a WESkit demo on your machine run steps 1, 2, 3 in the [One-Click Demo Deployment](./getting_started.md).

### SSH Credentials

WESkit requires the SSH credentials for the Slurm controller node in its config directory.

Copy the key and known_hosts file to the ``deployment/weskit_config/`` directory:
```bash
cp ~/.ssh/id_ed25519 deployment/weskit_config/
cp ~/.ssh/known_hosts deployment/weskit_config/
```

> NOTE: For local execution, SSH may reject the key if its permissions are too open (i.e., readable or writable by other users). However, this behavior differs when running inside a Docker container. If the user IDs on the host and inside the container do not match, it may be necessary to adjust the key file's permissions to allow readability by other users.

Ensure that the key file is owned by the WESkit user and has permissions set to 600 (readable only by the owner). Also make sure that its parent directories are not writable by group or others (permissions set to 700 or 755). If these permissions are not correctly set, SSH may refuse to use the key.

### Adjust config file

Apply the following changes to ``deployment/weskit_config/config.yaml`` :

> NOTE: Keep in mind that the paths are pointing to directories inside the docker container and the Slurm nodes.

```yaml
executor:
  type: "ssh_slurm"
  remote_data_dir: "/path/to/remote/data"
  remote_workflows_dir: "/path/to/remote/workflows"
  singularity_containers_dir: None
  login:
    username: "ubuntu"
    host: "XXX.XXX.XXX.XXX"
    port: 22
    knownhosts_file: "/home/weskit/weskit_config/known_hosts"
    keyfile: "/home/weskit/weskit_config/id_ed25519"
    keyfile_passphrase: ""
    keepalive_interval: "30s"
    keepalive_count_max: 5
```

Enter the configuration values depending on your setup:

* **type**: ``ssh_slurm`` to use Slurm as execution backend.
* **remote_data_dir**: Path to the data directory (*Slurm worker*). This will be used as working directory for the Runs.
* **remote_workflows_dir**: Path containing the workflow directory (*Slurm worker*). Make sure to copy your workflows to this directory.
* **singularity_containers_dir**: Leave ``None`` if you are not using a Singularity container.
* **username**: The Slurm user, which is going to submit the jobs.
* **host**: Hostname/IP of the slurm login node.
* **port**: SSH port, usually ``22``.
* **knownhosts_file**: Path to the knownhosts file (*worker docker container*).
* **keyfile** and **keyfile_passphrase**: Path and passphrase for the key file (*worker docker container*).
* **keepalive_interval** and **keepalive_count_max**: Keepalive settings.

### Environment script

You can configure WESkit to run a script before the actual command. This is intended for setting up the environment for the workflow. The environment script can be used e.g. to activate a Conda environment.

Create engine environment script:
```bash
#!/bin/bash
set +u 
source /home/ubuntu/miniforge3/bin/activate
conda activate weskit_deployment
set -u
```

The script must be copied readable by the Slurm worker node. 

To apply the script to the workflow, provide the path in the API call payload as ``engine_params.engine-environment``.

## Step 3: Deploy WESkit


Start WESkit stack:
```bash
docker swarm init
cd deployment/
python weskit_stack.py start
```

> NOTE: If WESkit was deployed already, you have to stop and [restart the stack](#apply-config-changes).

## Step 4: Test Run Submission

Submit a test workflow to ensure the Slurm backend is working as expected.

Find a step-by-step guide in the [Run submmission](../api/RunSubmission.md) document.

## Troubleshooting

This section contains some common issues and steps to fix them.

### Apply config changes

If you make changes to the config, you have to restart the WESkit stack to apply the changes.

Remove WESkit from the stack:
```bash
docker stack rm weskit
```

Restart the WESkit stack:
```bash
cd deployment/
python weskit_stack.py start
```

### WES API unreachable

If the REST-service is unreachable, you might have to forward the port of the REST docker container.

Add port 5000 in ``deployment/static/weskit_stack_base.yaml`` :
```yaml
services:
  rest:
    ...
    ports:
      - "5000:5000" 
```


### Create and apply a specific branch docker image
  The repository is under active development, therefore you might want to use a different docker image. You can deploy WESkit using an image from a different branch, by building the image locally and adjusting the docker compose file. In the following steps we will build a docker image from the ``custom_branch`` branch.
  
  Clone the WESkit API repository and switch to the branch:
  ```bash
  git clone https://gitlab.com/one-touch-pipeline/weskit/api.git
  git switch custom_branch
  ```

  Change the base image in ``api/Dockerfile`` (line 5):
  ```docker
  ARG baseImage=registry.gitlab.com/one-touch-pipeline/weskit/api/base:custom_branch
  ```

  Build the image:
  ```bash
  cd api/
  docker build -t custom_branch:1 .
  ```

  Change the image for REST and worker services in ``deployment/static/weskit_stack_base.yaml`` (line 23, 82):
  ```yaml
  services:
    rest:
      image: custom_branch:1
      ...
    worker:
      image: custom_branch:1
  ```

  Now, WESkit will be deployed using the image created from the ``custom_branch`` branch.


### Useful Commands:
- `scontrol show job <job_id>`: Detailed job information.
- `sacct -j <job_id>`: Job accounting data.
