# One-Click Demo Deployment

This describes the Docker Swarm based deployment of the WESkit application stack as implemented in the [deployment repository](https://gitlab.com/one-touch-pipeline/weskit/deployment).
The deployment repository contains a pre-configured demo deployment.
There are just a few things to do for users to get the full WESkit stack running on Docker Swarm.

## Preparation

1. Clone the deployment repository and install the deployment environment, which contains just a few required python packages.

```bash
git clone https://gitlab.com/one-touch-pipeline/weskit/deployment
cd deployment
conda env create -n weskit_deployment -f environment.yaml
conda activate weskit_deployment
```

2. Running the WESkit demo deployment mounts the `test/data` folder from the repository into the WESkit container.
All results will be written into this folder by a non-root user. Therefore, we need to set write access for this folder to 777.

```bash
chmod 777 tests/data
```

3. The pre-configured WESkit rest server and other components require certificates for secure communication.
The repository contains a script `generateDevelopmentCerts.sh` to create respective certificates which will be stored at `certs/`. The certificates will then be integrated as secrets into the container.

```bash
./generateDevelopmentCerts.sh
```

## Start Weskit

4. Now start the stack using the python script:

```bash
python weskit_stack.py start
```

This command will start the services in your Docker swarm.

## API demo

5. Once the stack is running (~1min), you can execute a demo script that submits a Snakemake workflow to WESkit.
Per default, the service and the dashboard will be available at ["https://localhost"]("https://localhost"). It might be necessary to resolve localhost manually to 127.0.0.1 .

```bash
python weskit_stack.py test
```

The processed workflow data will be stored within the `tests/data` folder.

## Additional services

The demo stack can also be deployed together with a Keycloak service for authorization and a MinIO service to provide S3 access to the demo data.

```bash
python weskit_stack.py start --login --minio
python weskit_stack.py test --login
```

After starting the stack, the services will be available with default credentials:
- Keycloak: https://localhost:8443/auth/ (username: `admin`, password: `admin`)
- MinIO: http://localhost:9001/login (username: `minioadmin`, password: `minioadmin`)

## Using Curl to access the API

The following demonstrates, how to use cURL to obtain an JWT and query the WESkit API.

```bash
weskitHost=localhost
weskitPort=5000
keycloakHost=localhost
keycloakPort=8090
keycloakClientId=""
keycloakClientSecret=""
weskitRealm=weskit-test

jwt="$(
curl \
  -v \
  --cacert ~/helm-deployment/certs/weskit.crt \
  -d grant_type='client_credentials' \
  -d client_id="$keycloakClientId" \
  -d client_secret="$keycloakClientSecret" \
  -X POST "http://$keycloakHost:$keycloakPort/realms/$weskitRealm/protocol/openid-connect/token" \
  | jq -r ".access_token" \
)"
curl \
  -v \
  --cacert ~/helm-deployment/certs/weskit.crt \
  --insecure \
  --ipv4 \
  --header 'Accept: application/json' \
  --header "Authorization: Bearer $jwt" \
  https://$weskitHost:$weskitPort/ga4gh/wes/v1/runs
```

This makes some assumptions about how keycloak and WESkit are configured, but should serve as an example that you can adapt to your testing needs.
