Here you find relevant information on about how to deploy and administer a WESkit instance. 

  * [Getting started](deployment/getting-started.md)
  * [Requirements](deployment/requirements.md)
  * [Configuration](deployment/configuration.md)
  * [Helm Deployment](deployment/helm.md)
  * [Docker-Compose Deployment](deployment/docker-compose.md) (outdated)
  * [Workflow Installation](deployment/workflow-installation.md)
  * [Demo Workflow Submission](deployment/demo_workflow_submission.md)
  * [Proxies](deployment/proxies.md)
  * [Multi-Node Swarm](deployment/multi-node-swarm.md)
  * [Kerberos](deployment/Kerberos.md)
  