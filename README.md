# WESkit Documentation

WESkit is an implementation of the [Workflow Execution Service API](https://github.com/ga4gh/workflow-execution-service-schemas) of the [Global Alliance for Genomic Health (GA4GH)](https://www.ga4gh.org/). The purpose of the API is to provide a standardized programmatic way to run and manage workflows. The goals of WESkit are

 * **Standard Conformance**: Fully implement the WES-API.
 * **Scalability**: Allow to run many workflows in parallel. This requirement guided the early design decisions, such as using Celery for executing workflow managers.
 * **Flexibility**: Work with more than a single workflow management system. Our current main focus is on [Nextflow](https://nextflow.io/) and [Snakemake](https://snakemake.github.io/), but it should be easy to implement others and this will certainly happen in the future.
 * **HTC Cluster**: WESkit should be usable in a high-throughput cluster system like Slurm or IBM LSF. For this we use the workflow management systems's cluster capabilities, and WESkit supports a shared filesystem infrastructure.
 * **Cloud**: This requires an easy deployment and should not depend on a shared filesystem infrastructure. Also, the failure-robustness requirements of a cloud infrastructure may be stronger than for an installation on premise.
 * **Production Quality**: We need a system that we can rely on, that requires little maintenance effort, and that is relatively easy to understand. Production quality also means that it should be easy to continue the development of the system for a long time.

## Getting started

You can follow our small tutorial to deploy an instance of WESkit on your local machine.

 * [Getting started](deployment/getting-started.md)


## Further information

You can find specific information in the following subsections:

 * [Technology](technology/README.md): documentation about the used technologies, components and features
 * [Deployment](deployment/README.md): docker stack deployment, configuration and administration of WESkit
 * [API](api/README.md): specificities of the WESkit API
 * [Team](Team.md): meet us and join the team!

## References

 * Kensche P.R., Schneider-Lunitz V., Baby C.S., Kanitz, A., Muffato, M., Twardziok, S., Buchhalter, I. (2023). WESkit: Workflow execution for HTC and cloud [version 1; not peer
   reviewed]. F1000Research 2023, 12(ELIXIR):1006 (poster) (doi: [2]10.7490/f1000research.1119541.1)
 * Kanitz, A., Baby, C. S., Buchhalter, I., Muffato, M., Kensche, P. R., Schneider-Lunitz, V., & Twardziok, S.  (in alphabetical order) (2022). [**WESkit: Flexible Workflow Execution for HTC and Cloud.**](documents/PKensche_WESkit_GA4GH_Plenary_2022_Poster_final.pdf). GA4GH 10th Plenary Meeting. Poster 20. https://broadinstitute.swoogo.com/ga4gh-10th-plenary/posters
 * Kensche, P. R., Kanitz, A., Topolsky, I., Jablonski, K. P., Mandreoli, P., & Twardziok, S. (2023). **Executing workflows in the cloud with WESkit.** BioHackrXiv, February 20. https://doi.org/https://doi.org/10.37044/osf.io/2z6nu
 * Twardziok, S. _et al._ (2022). **Orchestrate your Nextflow and Snakemake runs with WESkit**. ELIXIR All-Hands 2022. https://drive.google.com/file/d/1nr7GMafu3YXWt_WyRlELrKjv2CmzFV1m/view