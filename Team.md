# WESkit Team

## Current WESkit team

  * Landfried Kraatz [2]
  * Philip R. Kensche [1]
  * Sven Twardziok [2]
  * Valentin Schneider-Lunitz [2]

## Former team members

  * Cibin S. Baby [3]
  * Ben Wulf [2]
  * Elisabeth Bürger [1]
  * Jan Sahrhage [1]
  * Julian Pipart [2]

## Affiliations
 
  1. Deutsches Krebsforschungszentrum Heidelberg (DKFZ)
  2. Berlin Institute for Health (BIH)
  3. Wellcome Sanger Institute