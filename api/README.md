As API, we here consider the REST-API but also the behavior with respect to the produced results.

  * [Run Submission](api/RunSubmission.md)
  * [Run Directory](api/WorkingDirectory.md)  
  * [Standard output files](api/OutputFiles.md)
  * [States](api/States.md)