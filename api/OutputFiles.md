For every attempted execution of a workflow engine, WESkit creates a new run directory and 
writes logs into the log directory `.weskit/$timestamp/` located in that run-directory. The timestamp directory is of the format "%Y-%m-%dT%H:%M:%SZ", e.g. `2021-05-06T15:44:05Z`.

The following files are created in the log directory

  * `stdout`: The plain standard output of the workflow engine execution.
  * `stderr`: The plain standard error of the workflow engine execution.
  * `log.json`: Additional metadata of the run. 

Here is an example of the `log.json` (after piping through `jq`):

```json
{
  "start_time": "2021-05-06T15:44:05Z",
  "cmd": [
    "snakemake",
    "--snakefile",
    "Snakefile",
    "--cores",
    "1",
    "--configfile",
    "config.yaml"
  ],
  "workdir": "3baa/3baab2cf-4d63-4e24-9476-2a9245dde32c",
  "end_time": "2021-05-06T15:44:05Z",
  "exit_code": 0,
  "stdout": ".weskit/2021-05-06T15:44:05Z/stdout",
  "stderr": ".weskit/2021-05-06T15:44:05Z/stderr",
  "log_file": ".weskit/2021-05-06T15:44:05Z/log.json",
  "output_files": [
    "hello_world.txt",
    "config.yaml",
    ".snakemake/log/2021-05-06T154405.522126.snakemake.log",
    ".snakemake/metadata/aGVsbG9fd29ybGQudHh0"
  ]
}
```

Note that the "output_files" contains all files produced by the workflow run, including the `config.yaml` and files produced by the workflow engine, e.g. for bookkeeping. Not contained are the files in the `.weskit/` directory, though.