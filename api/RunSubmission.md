# Run submission

Workflows are submitted as runs via the POST request method and the required data is attached as a JSON object. Important here, the `workflow_params` field in data is defined as a nested JSON within the data JSON and not as a JSON string. WESkit stores the content of `workflow_params` as a `config.yaml` file inside the [working directory](api/WorkingDirectory). The workflow and workflow engine need to be able to read YAML files as config files.

# Example 

Here is an example of how to submit runs via Python:

```python
snakefile = "PATH/TO/WORKFLOW"
workflow_params = {"text":"hello world"}
weskit_host = "http://WESKIT_URL"

data = {
        "workflow_params": workflow_params,
        "workflow_type": "snakemake",
        "workflow_type_version": "7.30.2",
        "workflow_url": snakefile
    }
requests.post("%s/ga4gh/wes/v1/runs" % (weskit_host), json=data)
```

NOTE: Some workflow-engine parameters can be configured via API calls. Additionally, you may set options using a `nextflow.config` file uploaded as "workflow_attachment".
# Queue, project and group configuration

A run consists of two different types of jobs:

1. **Control/engine jobs** (Snakemake, Nextflow)
2. **Workload jobs** (the jobs submitted by the control/engine jobs)

You can run control/engine jobs and workload jobs in separate queues for balancing the cluster resources. You may also want to set the project/account and group of the jobs. In the following, we will describe how these properties can be configured for SLURM and LSF.

## SLURM

This section describes the configuration of *queue* and *project* if WESkit is using a SLURM cluster as execution backend. The configuration of a *group* is not natively supported by SLURM. The following table contains the corresponding SLURM and WESkit parameters for each property:

| WESkit parameter | SLURM parameter | Description |
| :--------------- | :-------------- | :---------- |
| queue            | partition       | Set of compute nodes configured with specific access controls, resource limits, and scheduling policies to manage job execution. |
| accounting-name  | account         | Used to enforce resource usage limits, track job accounting, and manage access controls.  |
| group            | -               | Not available in SLURM. |

Further reading: 
* [SLURM parameters](https://slurm.schedmd.com/sbatch.html) (for *sbatch*)
* [Architecture](https://slurm.schedmd.com/quickstart.html#arch) (SLURM partions etc.)

### Control/Engine jobs

The parameters for the control/engine jobs can be configured either as a default parameter for all jobs, or specific for each run. Parameters specified via API calls take precedence over default parameters defined in the config file.

#### Option 1: Configuration via default parameters

The default queue for all control/engine jobs can be set in the WESkit config . It is defined by the *queue* default parameter of the workflow engine (`NFL` or `SMK`). To set the project/account, use the parameter *accounting-name*. If you want to make a parameter configurable via API call, set `api: true`. 

The WESkit config file could be found for example in:
* `deployment/weskit_config/config.yaml` for Docker Swarm deployment 
* `configs/weskit/config.yaml.tpl` for Kubernetes Helm deployment 

> NOTE: The parameter `group` can be set in the same way. However, SLURM does not implement support groups, so this will have no effect on the submitted jobs.

Add the default parameters `queue` and `accounting_name` to your **config.yaml**:
``` yaml
workflow_engines:
  NFL:
    "23.04.1": 
      default_parameters:
        - name: "queue"
          value: "partition_1"
          api: true
          type: "Optional[str]"
        - name: "accounting-name"
          value: "account_1"
          api: true
          type: "Optional[str]"
```

This example sets the default parameters for Nextflow. If you are using Snakemake you can use the same parameters in the "workflow_engines.SMK" section of the config.yaml file.

#### Option 2: Configuration via API 

To set the parameters for a specific run, you can specify them in the API call. They are set inside the `workflow_engine_parameters`.

Add the default parameters `queue` and `accounting_name` to your API call data payload:
```python
data = {
    "workflow_params": '{"text": "hello world"}'
    "workflow_url": "file:tests/wf1/Snakefile"
    "workflow_type": "SMK"
    "workflow_type_version": "7.30.2"
    "workflow_engine_parameters":
        {
            "accounting-name": "my-project",
            "group": "my-group",
            "queue": "my-queue"
        }
}
```

### Nextflow workload jobs

If you are running a Nextflow workflow, you can set the partition/queue independently for each workload process. In the following, we will show 2 ways to configure the partition/queue of a Nextflow process. 

For more information, refer to the [official Nextflow documentation](https://nextflow-io.github.io/elixir-workshop-21/docs/fifth.html#slurm).

> NOTE: Remember that SLURM's *partition* is equivalent to the *queue* parameter in Nextflow syntax.

#### Option 1: Modify process directly

You can specify the slurm partition directly in the Nextflow file by adding the queue directive within each process block. 

In your `workflow.nf` label the process:
```groovy
process example_process {
    executor 'slurm'
    queue 'partition_1'

    script:
      // ...
}
```
#### Option 2: Use nextflow.config

You can specify a *nextflow.config* file, that contains the process parameters.

> NOTE: Nextflow config files are currently not supported by WESkit. As a workaround, configure process-specific settings directly in the workflow file using Option 1 above.

In your `nextflow.config` set the process parameters:
```groovy
process {
    executor = 'slurm'
    queue = 'partition_1'
}
```
If you need processes in a workflow running in different partitions, you can create labels containing the queue information. These labels can be added to the processes to set their queue parameter.

In your `nextflow.config` create a label:
```groovy
process {
    withLabel: 'process_group1' {
        executor = 'slurm'
        queue = 'partition_1'
    }
}
```

In `yourProcess.nf` label the process:
```groovy
process yourProcess {
    label 'process_group1'
    script:
    // ...
}
```

### Snakemake workload jobs

If you are running a Snakemake workflow, you can set the partition/queue independently for each workload process. In the following, we will show how to configure the partition/queue of a Snakemake process by creating a profile.

Find details about launching Snakemake workflows in SLURM in the [official Snakemake docs](https://snakemake.readthedocs.io/en/v7.30.1/executing/cluster.html#cluster-slurm).

#### Step 1: Create the profile

Snakemake looks for the profile config in several locations. We recommend placing it in the separate folder under the *WESkit workflow directory*. If you create a profile named "slurm" to be used by the "wf1" workflow, you would place it in */weskit/workflows/wf1/slurm*. The config file itself has to be named *config.yaml*. 

Therefore, create a file `/weskit/workflows/wf1/slurm/config.yaml` with the following content:
```yaml
cluster: "sbatch --parsable --partition={resources.partition} --cpus-per-task=1"
jobs: 2
default-resources:
  partition="debug"
```
In this file, we
- set the command to spawn a job for a rule ("sbatch ...")
- set the maxmium number of jobs launched by the workflow
- set a default partition

If a rule does not set a partition, it will be launched using the default partition.

#### Step 2: Configure workflows

Let's assume we have a rule "step1" in our Snakefile. With our "slurm" profile, we can simply define the partition to be used for this rule in the resources section.

Set the `resources.partition` parameter in your Snakefile:
```yaml
rule step1:
    resources:
        partition="partition_1"
    shell:
        # ...
```
If unset, the rule will be executed on the partition defined inside the *default-resources* in the profile.


#### Step 3: Apply profile in WESkit

To activate the profile in WESkit, you must set it in the *workflow_engine_parameters* of the API call data. 

Activate your profile in `workflow_engine_parameters.profile` in the API call data:
```python
data = {
    "workflow_engine_parameters":
        {
            "profile": "slurm"
        }
}
```

> NOTE: The *profile* parameter must be configurable via API call. Add it to the `default_parameters` section in the WESkit *config.yaml* with `api: true`, similar to the queue configuration shown in Option 1 above.

## LSF

> NOTE: LSF configuration documentation is currently under development and will be added in a future update.