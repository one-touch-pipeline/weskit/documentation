WESkit can be operated in one of two modes.

### GA4GH-WES Compatible Mode

If `require_workdir_tag` in the `weskit.yaml` is set to `false`, then WESkit creates run-directories using random UUIDs. The run directories are put into WESKIT_DATA and are named `$uuid4prefix/$uuid`, where `$uuid4prefix` are the first 4 letters in the `$uuid`.

### Shared-Filesystem Mode

This mode was specifically designed to cope with the second use-case that WESkit was designed for, namely as a component of a data management and automation system. In this use case, the data is managed on an NFS that can be accessed by the REST-client, the REST-server, the Celery workers running the workflow managers, and the bioinformatic (cluster) jobs, submitted by the workflow managers.

If `require_workdir_tag` is set to `true`, then the working directory for every run can be specified by the REST client application via the API. This allows -- and requires -- a tighter behavioural integration of WESkit with the client:

  * The work-directory needs to be present before the workflow submission. WESkit will just use the directory provided by the client.
  * The client, WESkit, the celery workers, and the submitted cluster jobs need to be allowed to write to the run-directory.
  * The REST-client can manage the permissions for the run-directory directory.
  * The client keeps the ownership of the run-directory. WESkit will usually be configured to prune run-directories from its database after a certain period of time.

The run-directory needs to be set in the "run_dir" tag during workflow submission. E.g. (multipart/form-data)

```
workflow_params="[]"
workflow_type=snakemake"
workflow_type_version=7.30.2"
workflow_url="file:workflows/fancy-workflow/Snakefile"
tags='{ "run_dir": "file:path/to/rundir" }'
```
