# Description of the WES States

Set by WESkit:
 - **RUN_CREATED**: WESkit creates a Run. Maybe there is a directory, and maybe the attachment files were partially written to the run-dir.

 - **PREPARED_EXECUTION**: A Celery task ID was defined and the execution may or may not have started.

 - **SUBMITTED_EXECUTION**: The execution of the workflow engine execution has been submitted to the Celery task with the known ID.

 - **SYSTEM_ERROR**: Any error of the WESkit system itself. Examples are:
    * System error in task not resulting in Celery task FAILURE
    * Errors in infrastructure (DB, Redis, Keycloak, etc.)
    * Filesystem errors (inaccessible mounts, etc.), other than client-caused file access errors (e.g. wrong paths).
    * WESkit.Executor errors (cluster errors, SSH errors, etc.)

 - **EXECUTOR_ERROR**: The task encountered an error in one of the Executor processes.

Defined by Celery worker:

- **AWAITING_START**: Celery task is submitted. This may be a run-task representing a workflow engine run but may also include a preparatory task to stage large request-attachments, etc.

 - **STARTED_EXECUTION**: An "Executor" seems to be executing individual workfload jobs ("first Executor", etc.) WESkit has no access to these Executors, which are managed by the workflow engines. Thus, the RUNNING state is assumed, as long as the workflow engine runs.

 - **FINISHED_EXECUTION**: The Celery task finished (successfully or not) All workload jobs and the workflow engine executed with exit code == 0.

 - **EXECUTOR_ERROR**: The task encountered an error in one of the Executor processes. Generally, this means that an Executor exited with a non-zero exit code. exit_code > 0

 - **CANCELED**: The workflow engine run (~ task) has been successfully cancelled.

 - **REQUESTED_CANCEL**:  The workflow engine run (~ task) is being cancelled, e.g. waiting for the engine to respond to SIGTERM and clean up running cluster jobs, compiling incomplete run, run results, etc.

Not set:
 - **PAUSED**: The workflow run is paused. Not implemented yet.

# Transitions

WESkit controls possible state transitions. Following state changes may occur during lifetime of a single run:

 - (**RUN_CREATED**, **PREPARED_EXECUTION**) to (**SUBMITTED_EXECUTION**, **AWAITING_START**, **STARTED_EXECUTION**, **FINISHED_EXECUTION**, **EXECUTOR_ERROR**)

 - **SUBMITTED_EXECUTION** to (**AWAITING_START**, **STARTED_EXECUTION**, **FINISHED_EXECUTION**, **EXECUTOR_ERROR**)

 - **AWAITING_START** to (**AWAITING_START**,**STARTED_EXECUTION**, **FINISHED_EXECUTION**, **EXECUTOR_ERROR**)

 - **STARTED_EXECUTION** to (**STARTED_EXECUTION**, **FINISHED_EXECUTION**, **EXECUTOR_ERROR**)

 - **FINISHED_EXECUTION** to (**FINISHED_EXECUTION**, **EXECUTOR_ERROR**)

 - **REQUESTED_CANCEL** to **CANCELED**


```plantuml
@startuml

scale 600 width

state INITIALIZING #aaffaa
state QUEUED #aaffaa
state RUNNING #aaffaa
state COMPLETE #aaffaa
state EXECUTOR_ERROR #ffaaaa
state CANCELING #aaffff
state CANCELED #aaffff
state SYSTEM_ERROR #ffaaaa

[*] -down-> INITIALIZING
INITIALIZING -down-> QUEUED
INITIALIZING -down--> RUNNING
INITIALIZING -down---> COMPLETE
INITIALIZING -down--> EXECUTOR_ERROR
INITIALIZING -down--> SYSTEM_ERROR

QUEUED -> QUEUED
QUEUED -down--> RUNNING
QUEUED -down---> COMPLETE
QUEUED -down--> CANCELING
QUEUED -down--> EXECUTOR_ERROR
QUEUED -down--> SYSTEM_ERROR

RUNNING -> RUNNING
RUNNING -> QUEUED
RUNNING -down--> COMPLETE
RUNNING -left-> CANCELING
RUNNING -right-> EXECUTOR_ERROR
RUNNING -right-> SYSTEM_ERROR

CANCELING -down-> CANCELED

EXECUTOR_ERROR -[hidden]down-> SYSTEM_ERROR

COMPLETE -down-> [*]
CANCELED -down-> [*]
SYSTEM_ERROR -down-> [*]
EXECUTOR_ERROR -down-> [*]

@enduml
```